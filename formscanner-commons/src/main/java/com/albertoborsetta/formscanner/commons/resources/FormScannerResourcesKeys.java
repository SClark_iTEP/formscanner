package com.albertoborsetta.formscanner.commons.resources;

public class FormScannerResourcesKeys {

	public static final String OPEN_IMAGES_ICON = "open";

	public static final String SAVE_ICON = "save";

	public static final String RENAME_FILES_ICON = "rename";

	public static final String ANALYZE_FILES_ICON = "start";

	public static final String ANALYZE_FILES_ALL_ICON = "start_all";

	public static final String REMOVE_FIELD_BUTTON = "fewer";

	public static final String ADD_FIELD_BUTTON = "more";

	public static final String DISABLED_BUTTON = "disabled";

	public static final String ENABLED_BUTTON = "enabled";

	public static final String ANALYZE_FILES_CURRENT_ICON = "reload";
	
	public static final String FORMSCANNER_SPLASH = "splash";

	public static final String EXIT_ICON = "exit";

	public static final String EDIT_ICON = "edit";

	public static final String CONFIG_ICON = "config";

	public static final String HELP_ICON = "help";
	
	public static final String LANGUAGE_ICON = "spelldialog";
	
	public static final String ABOUT_ICON = "about";
	
	public static final String FORMSCANNER_ICON = "formscanner";
	
	public static final String IMPORT_ICON = "import";

	public static final String CLEAR_CONSOLE_ICON = "clear";

	public static final String HIDE_PANEL_ICON = "hide";

	public static final String SHOW_PANEL_ICON = "show";

	public static final String CONSOLE_ICON = "console";

	public static final String IMAGES_ICON = "image";

	public static final String TEMPLATE_ICON = "template";

	public static final String ZOOM = "zoom";

	public static final String CLEAR_TEMPLATE_BUTTON = "clear";

	public static final String CLEAR_IMAGES_BUTTON = "clear";
}
